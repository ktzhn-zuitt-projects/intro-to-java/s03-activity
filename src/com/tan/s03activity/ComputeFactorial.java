package com.tan.s03activity;

import java.util.Scanner;

public class ComputeFactorial {

    public static void main (String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        System.out.println("What number would you like to input? ");
        long factor = 1;
        int i = 1;

        try {
            int num = Integer.parseInt(in.nextLine());
            for (i = 1; i <= num; i++) {
                factor *= i;
            }

            if (num < 0 ) {
                throw new Exception();
            }
            if (num == 0 ) {
                int zero = 1;
                factor = zero;
            }
            if (factor < 0 ) {
                System.out.printf("The factorial of %d is equal to" + " " + -factor + ".", num);
            }
            else {
                System.out.printf("The factorial of %d is equal to %d.", num, factor);
            }
        }
            catch (NumberFormatException error) {
                System.out.println("You did not input a number. Try again.");

            }
            catch (Exception e) {
                    System.out.println("Factorials cannot be computed for negative numbers. Please enter a positive number.");
                }
    }
    }

